package ru.org.ttproject;

import lib.parents.User1;
import org.junit.Test;

public class GettingProfilePageTest extends User1{

    @Test
    public void testGettingProfilePage(){
        profilePage = mainPage.goToProfilePageFromHeader();
        profilePage.isResumeRedactingBtnDisplayed();
    }
}
