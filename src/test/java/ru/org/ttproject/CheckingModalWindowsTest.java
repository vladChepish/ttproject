package ru.org.ttproject;

import lib.parents.User1;
import org.junit.Test;

public class CheckingModalWindowsTest extends User1 {

    @Test
    public void testCheckingModalWindowResume(){
        profilePage = mainPage.goToProfilePageFromHeader();
        profilePage.isResumeModalWindowDisplayed();
        profilePage.isSceduleModalWindowDisplayed();
        profilePage.isContactsModalWindowDisplayed();
        profilePage.isDeviceModalWindowDisplayed();
    }
}
