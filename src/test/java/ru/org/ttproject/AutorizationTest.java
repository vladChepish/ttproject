package ru.org.ttproject;

import lib.parents.NoUser;
import org.junit.Test;

public class AutorizationTest extends NoUser{

    @Test
    public void testAutorization(){
        mainPage.compareInputsInLoginForm();
        mainPage.login(user1.getUserName(), user1.getPassword());
        mainPage.checkingIsLoginSuccessful();
    }
}
