package ru.org.ttproject;

import lib.parents.User1;
import org.junit.Test;

import static org.junit.Assert.*;

public class SavingInfoWithModalWindow extends User1 {

    @Test
    public void testSavingInfoWithModalWindow(){
        String shouldBePosition = "Технический директор";
        String shouldBeShortResume = "Этот текст будет использоваться в качестве альтернативного, для замены по ходу теста";

        profilePage = mainPage.goToProfilePageFromHeader();
        String positionBefore = profilePage.getPosition();
        String shortResumeBefore = profilePage.getShortResume();

        profilePage.openResumeModalWindow();

        profilePage.setNewInformationInResumeModalWindow(shouldBePosition, shouldBeShortResume);
        profilePage.saveInformationIntoModalWindow();

        String positionAfter = profilePage.getPosition();
        String shortResumeAfter = profilePage.getShortResume();

        assertEquals("Position doesn't match", shouldBePosition, positionAfter);
        assertEquals("Short resume doesn't match", shouldBeShortResume, shortResumeAfter);

        profilePage.refresh();
        profilePage.openResumeModalWindow();

        String posititonInModalWindow = profilePage.getPositionFromModalWindow();
        String shortResumeInModalWindow = profilePage.getShortResumeFromModalWindow();
        assertEquals("Position in modal window doesn't match", positionAfter, posititonInModalWindow);
        assertEquals("Short resume in modal window doesn't match", shortResumeAfter, shortResumeInModalWindow);

        profilePage.setNewInformationInResumeModalWindow(positionBefore, shortResumeBefore);
        profilePage.saveInformationIntoModalWindow();
    }

}
