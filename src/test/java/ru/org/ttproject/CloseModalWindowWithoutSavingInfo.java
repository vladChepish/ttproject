package ru.org.ttproject;

import lib.parents.User1;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CloseModalWindowWithoutSavingInfo extends User1 {

    @Before
    public void beforeMethod(){
        profilePage = mainPage.goToProfilePageFromHeader();
    }

    @Test
    public void testClosingModalWindowWithoutSavingBeXBtn(){
        String positionForRepleace = "Технический директор";
        String shortResumeForRepleace = "Этот текст будет использоваться в качестве альтернативного, для замены по ходу теста";

        String positionBefore = profilePage.getPosition();
        String shortResumeBefore = profilePage.getShortResume();

        profilePage.openResumeModalWindow();

        profilePage.setNewInformationInResumeModalWindow(positionForRepleace, shortResumeForRepleace);
        profilePage.closeModalWindowByXBtn();

        String positionAfter = profilePage.getPosition();
        String shortResumeAfter = profilePage.getShortResume();
        assertEquals("Position doesn't match", positionBefore, positionAfter);
        assertEquals("Short resume doesn't match", shortResumeBefore, shortResumeAfter);

        profilePage.refresh();
        profilePage.openResumeModalWindow();

        String posititonInModalWindow = profilePage.getPositionFromModalWindow();
        String shortResumeInModalWindow = profilePage.getShortResumeFromModalWindow();
        assertEquals("Position in modal window doesn't match", positionBefore, posititonInModalWindow);
        assertEquals("Short resume in modal window doesn't match", shortResumeBefore, shortResumeInModalWindow);
        profilePage.closeModalWindowByXBtn();
    }

    @Test
    public void testClosingModalWindowWithoutSavingByCloseBtn(){
        String positionForRepleace = "Технический директор";
        String shortResumeForRepleace = "Этот текст будет использоваться в качестве альтернативного, для замены по ходу теста";

        String positionBefore = profilePage.getPosition();
        String shortResumeBefore = profilePage.getShortResume();

        profilePage.openResumeModalWindow();

        profilePage.setNewInformationInResumeModalWindow(positionForRepleace, shortResumeForRepleace);
        profilePage.closeModalWindowByCloseWithoutSavingBtn();

        String positionAfter = profilePage.getPosition();
        String shortResumeAfter = profilePage.getShortResume();
        assertEquals("Position doesn't match", positionBefore, positionAfter);
        assertEquals("Short resume doesn't match", shortResumeBefore, shortResumeAfter);

        profilePage.refresh();
        profilePage.openResumeModalWindow();

        String posititonInModalWindow = profilePage.getPositionFromModalWindow();
        String shortResumeInModalWindow = profilePage.getShortResumeFromModalWindow();
        assertEquals("Position in modal window doesn't match", positionBefore, posititonInModalWindow);
        assertEquals("Short resume in modal window doesn't match", shortResumeBefore, shortResumeInModalWindow);
        profilePage.closeModalWindowByCloseWithoutSavingBtn();
    }

    @After
    public void afterMethod(){
        mainPage.refresh();
    }
}
