package ru.org.ttproject;

import lib.parents.User1;
import org.junit.Test;

public class OpeningPreviewResume extends User1 {

    @Test
    public void testOpeningResumePreview(){
        profilePage = mainPage.goToProfilePageFromHeader();
        profilePage.openPreviewResume();
    }
}
