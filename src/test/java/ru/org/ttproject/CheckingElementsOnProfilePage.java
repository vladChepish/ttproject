package ru.org.ttproject;

import lib.parents.User1;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CheckingElementsOnProfilePage extends User1 {

    @Before
    public void beforeMethod(){
        profilePage = mainPage.goToProfilePageFromHeader();
    }

    @Test
    public void testCheckingResumeBlock(){
        profilePage.checkingResumeBlock();
    }

    @Test
    public void testCheckingSceduleBlock(){
        profilePage.checkingSceduleBlock();
    }

    @Test
    public void testCheckingContactsBlock(){
        profilePage.checkingContactBlock();
    }

    @Test
    public void testCheckingDeviceBlock(){
        profilePage.checkingDeviceBlock();
    }

    @After
    public void afterMethod(){
        mainPage.refresh();
    }
}
