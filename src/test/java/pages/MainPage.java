package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertTrue;

public class MainPage extends BasePage {

    /** Login/Main page */
    protected static final By LOGIN_INPUT = By.cssSelector("input[id='username']");
    protected static final By PASS_INPUT = By.cssSelector("input[id='password']");
    protected static final By LOGIN_BTN = By.cssSelector("input[id='_submit']");
    protected static final By LOGIN_FORM = By.cssSelector("form[action='/login_check']");

    /** User Cabinet Main Page */
    protected static final By USER_ICON_IN_HEADER = By.cssSelector("span.m-topbar__userpic");
    protected static final By USER_ICON_BTN_IN_HEADER = By.cssSelector("li[data-dropdown-toggle='click']");
    protected static final By MY_PROFILE_LINK_IN_HEADER = By.xpath("//div[@class='m-dropdown__body']//a[contains(@href, '/show/profile')]");

    public MainPage(WebDriver driver) {
        super(driver);
        shortWait.until(ExpectedConditions.visibilityOfElementLocated(LOGIN_FORM));
    }

    public void compareInputsInLoginForm() {
        assertTrue("Login input isn't displayed", isElementPresent(LOGIN_INPUT));
        assertTrue("Password input isn't displayed", isElementPresent(PASS_INPUT));
        assertTrue("Login button isn't displayed", isElementPresent(LOGIN_BTN));
    }

    public void login(String login, String pass) {
        fillField(LOGIN_INPUT, login);
        fillField(PASS_INPUT, pass);
        click(LOGIN_BTN);
    }

    public void checkingIsLoginSuccessful() {
        shortWait.until(ExpectedConditions.visibilityOfElementLocated(USER_ICON_IN_HEADER));
        assertTrue("Logi wasn't successful", isElementPresent(USER_ICON_IN_HEADER));
    }

    public MyProfilePage goToProfilePageFromHeader() {
        click(USER_ICON_BTN_IN_HEADER);
        shortWait.until(ExpectedConditions.visibilityOfElementLocated(MY_PROFILE_LINK_IN_HEADER));
        click(MY_PROFILE_LINK_IN_HEADER);

        return new MyProfilePage(driver);
    }
}
