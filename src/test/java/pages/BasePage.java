package pages;

import lib.Timeout;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait shortWait;
    protected WebDriverWait loadingWait;

    public BasePage(WebDriver driver){
        this.driver = driver;
        this.shortWait = new WebDriverWait(driver, Timeout.shortTimeout);
        this.loadingWait = new WebDriverWait(driver, Timeout.loading);
    }

    public boolean isElementPresent(By by){
        try {
            getElement(by).isDisplayed();
            return true;
        } catch (NoSuchElementException e){
            return false;
        }
    }

    public WebElement getElement(By by){
        return driver.findElement(by);
    }

    public List<WebElement> getElements(By locator) {
        try {
            return driver.findElements(locator);
        } catch (InvalidSelectorException e) {
            return new ArrayList<WebElement>(); // empty list, chrome case
        }
    }

    public void click(By by){
        getElement(by).click();
    }

    public void fillField(By by, String text){
        getElement(by).click();
        getElement(by).clear();
        getElement(by).sendKeys(text);
    }

    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            //
        }
    }

    public WebElement scrollIntoView(WebElement element) {
        executeScript("arguments[0].scrollIntoView(true);", element);
        executeScript("scroll(0,-200)");
        sleep(300);
        return element;
    }

    public Object executeScript(String script, Object... args) {
        try {
            return ((JavascriptExecutor) driver).executeScript(script, args);
        } catch (WebDriverException we) {
            fail("Script can't be executed");
        }
        return null;
    }

    public BasePage refresh() {
        driver.navigate().refresh();
        sleep(7000);
        return this;
    }

    public String getTextByLocator(By by) {
        return getElement(by).getText();
    }
}
