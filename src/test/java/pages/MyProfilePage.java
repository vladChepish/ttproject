package pages;

import lib.BaseClass.BaseClass;
import lib.Timeout;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class MyProfilePage extends BasePage {

    protected static final By CREATE_RESUME = By.cssSelector("div.m--pull-left a");
    protected static final By PHOTO_ON_PAGE = By.cssSelector("div.avatarCover img");

    protected static final By RESUME_BLOCK = By.cssSelector(".m-portlet:nth-of-type(2)");
    protected static final By RESUME_HEADER = By.xpath("//h3[contains(text(),'Резюме')]");
    protected static final By REDACT_RESUME_BTN = By.cssSelector("button[data-target='#popup-edit-resume']");
    protected static final By RESUME_POSITION = By.cssSelector("div.m-list-search__result-item:nth-of-type(1) span:nth-of-type(1)");
    protected static final By RESUME_POSITION_TEXT = By.cssSelector("div.m-list-search__result-item:nth-of-type(1) span:nth-of-type(2)");
    protected static final By SHORT_RESUME_FIELD = By.cssSelector("div.m-list-search__result-item:nth-of-type(2) span:nth-of-type(1)");
    protected static final By SHORT_RESUME_TEXT = By.cssSelector("div.m-list-search__result-item:nth-of-type(2) span:nth-of-type(2)");
    protected static final By RESUME_POSITION_SELECT = By.cssSelector("select[id='post-id'] option[selected]");
    protected static final By SHORT_RESUME_TEXT_AREA = By.cssSelector("textarea[id='post-description']");
    protected static final By SAVE_RESUME_CHANGES = By.cssSelector("button.update-post-info");
    protected static final By RESUME_MODAL_CLOSE_BY_X_BTN = By.cssSelector("div[id='popup-edit-resume'] button.close");
    protected static final By RESUME_MODAL_CLOSE_BY_CLOSE_BTN = By.xpath("//div[@id='popup-edit-resume']//button[contains(text(),'Закрыть без сохранения')]");

    protected static final By GET_RESUME_PREVIEW_BTN = By.xpath("//a[contains(text(), 'Сформировать резюме')]");

    protected static final By SCEDULE_BLOCK = By.cssSelector(".m-portlet:nth-of-type(3)");
    protected static final By SCEDULE_HEADER = By.xpath("//h3[contains(text(),'График работы')]");
    protected static final By REDACT_SCEDULE_BTN = By.cssSelector("button.btn_edit_schedule");
    protected static final By STACK_SCEDULE = By.cssSelector(".stack-schedule");

    protected static final By CONTACTS_BLOCK = By.cssSelector(".m-portlet:nth-of-type(4)");
    protected static final By CONTACTS_HEADER = By.xpath("//h3[contains(text(),'Контакты')]");
    protected static final By REDACT_CONTACT_BTN = By.cssSelector("button[data-target='#popup-edit-contact']");
    protected static final By CONTACTS_FIELD_ONE = By.cssSelector(".m-list-search__results a:nth-of-type(1) span:nth-of-type(1)");
    protected static final By CONTACTS_FIELD_TWO = By.cssSelector(".m-list-search__results a:nth-of-type(2) span:nth-of-type(1)");
    protected static final By CONTACTS_FIELD_THREE = By.cssSelector(".m-list-search__results a:nth-of-type(3) span:nth-of-type(1)");
    protected static final By CONTACTS_FIELD_FOUR = By.cssSelector(".m-list-search__results a:nth-of-type(4) span:nth-of-type(1)");
    protected static final By CONTACTS_FIELD_FIVE = By.cssSelector(".m-list-search__results a:nth-of-type(5) span:nth-of-type(1)");
    protected static final By CONTACTS_FIELD_SIX = By.cssSelector(".m-list-search__results a:nth-of-type(6) span:nth-of-type(1)");
    protected static final By CONTACTS_FIELD_SEVEN = By.cssSelector(".m-list-search__results a:nth-of-type(7) span:nth-of-type(1)");

    protected static final By RESUME_MODAL_WINDOW = By.cssSelector("div[id='popup-edit-resume'] div.modal-content");

    protected static final By SCEDULE_MODAL_WINDOW = By.cssSelector("div[id='popup-edit-schedule'] div.modal-content");

    protected static final By CONTACTS_MODAL_WINDOW = By.cssSelector("div[id='popup-edit-contact'] div.modal-content");

    protected static final By ADD_DEVICE_MODAL_WINDOW = By.cssSelector("div[id='popup-add-environment'] div.modal-content");

    protected static final By DEVICE_BLOCK = By.cssSelector(".m-portlet:nth-of-type(5)");
    protected static final By DEVICE_BLOCK_HEADER = By.xpath("//h3[contains(text(),'Окружения')]");
    protected static final By ADD_DEVICE_BTN = By.cssSelector("button.add-environment");
    protected static final By DEVICE_TABLE = By.cssSelector("#environment-container");

    protected static final By SUCCESS_MESSAGE = By.cssSelector("div.toast-success");

    public MyProfilePage(WebDriver driver) {
        super(driver);
    }

    public void isResumeRedactingBtnDisplayed() {
        assertTrue("Button 'Редактировать резюме' isn't displayed", isElementPresent(REDACT_RESUME_BTN));
    }

    public void checkingMakeResumeBtn() {
        assertTrue("Wrong button class", getElement(CREATE_RESUME).getAttribute("class").contains("btn-brand"));
    }


    public void checkkingPhotoPosition() {
        int highestPointPhoto = getElement(PHOTO_ON_PAGE).getLocation().y;
        int highestPointResumeBlock = getElement(RESUME_BLOCK).getLocation().y;
        assertTrue("Photo displayed on the wrong place", highestPointPhoto < highestPointResumeBlock);
    }

    public void checkingResumeBlock(){
        List<By> fieldsInsideList = Arrays.asList(RESUME_POSITION, SHORT_RESUME_FIELD);
        List<String> equalingDatas = Arrays.asList("Должность", "Краткое резюме");
        blockElementsCheckingWithEquals(RESUME_BLOCK, RESUME_HEADER, REDACT_RESUME_BTN, fieldsInsideList, equalingDatas);
    }

    public void checkingContactBlock(){
        List<By> fieldsInsideList = Arrays.asList(CONTACTS_FIELD_ONE, CONTACTS_FIELD_TWO, CONTACTS_FIELD_THREE, CONTACTS_FIELD_FOUR, CONTACTS_FIELD_FIVE, CONTACTS_FIELD_SIX, CONTACTS_FIELD_SEVEN);
        List<String> equalingDatas = Arrays.asList("E-mail корпоративный", "E-mail дополнительный", "Скайп", "Телефон (основной)", "Телефон (дополнительный)", "График", "Адрес");
        blockElementsCheckingWithEquals(CONTACTS_BLOCK, CONTACTS_HEADER, REDACT_CONTACT_BTN, fieldsInsideList, equalingDatas);
    }

    public void checkingSceduleBlock(){
        List<By> fieldsInsideList = Arrays.asList(STACK_SCEDULE);
        blockElementsChecking(SCEDULE_BLOCK, SCEDULE_HEADER, REDACT_SCEDULE_BTN, fieldsInsideList);
    }

    public void checkingDeviceBlock(){
        List<By> fieldsInsideList = Arrays.asList(DEVICE_TABLE);
        blockElementsChecking(DEVICE_BLOCK, DEVICE_BLOCK_HEADER, ADD_DEVICE_BTN, fieldsInsideList);
    }

    public void blockElementsCheckingWithEquals(By checkedBlock, By checkedBlockHeader, By checkedBlockBtn, List<By> checkidBlockEemetsInside, List<String> equalingDatas){
        SoftAssertions softAssert = new SoftAssertions();

        scrollIntoView(getElement(checkedBlock));
        softAssert.assertThat(isElementPresent(checkedBlockHeader))
                .as("Don't displayed blocks header with locator: '" + checkedBlockHeader +"'.")
                .isTrue();
        softAssert.assertThat(isElementPresent(checkedBlockBtn))
                .as("Don't displayed blocks button with locator: '" + checkedBlockBtn +"'.")
                .isTrue();

        for (int i = 0; i < checkidBlockEemetsInside.size(); i++){
            By by = checkidBlockEemetsInside.get(i);
            String equalingData = equalingDatas.get(i);
            softAssert.assertThat(getElement(by).getAttribute("data-original-title"))
                    .as("No visible element displayed with locator: '" + by +"'")
                    .isEqualTo(equalingData);
        }

        softAssert.assertAll();
    }

    public void blockElementsChecking(By checkedBlock, By checkedBlockHeader, By checkedBlockBtn, List<By> checkidBlockEemetsInside){
        SoftAssertions softAssert = new SoftAssertions();

        scrollIntoView(getElement(checkedBlock));
        softAssert.assertThat(isElementPresent(checkedBlockHeader))
                .as("Don't displayed blocks header with locator: '" + checkedBlockHeader +"'.")
                .isTrue();
        softAssert.assertThat(isElementPresent(checkedBlockBtn))
                .as("Don't displayed blocks button with locator: '" + checkedBlockBtn +"'.")
                .isTrue();
        for (By by : checkidBlockEemetsInside){
            softAssert.assertThat(isElementPresent(by))
                    .as("No visible element displayed with locator: '" + by +"'.")
                    .isTrue();
        }

        softAssert.assertAll();
    }

    public void isResumeModalWindowDisplayed() {
        checkingOpeningMadolWindow(RESUME_BLOCK, REDACT_RESUME_BTN, RESUME_MODAL_WINDOW);
    }

    public void isSceduleModalWindowDisplayed() {
        checkingOpeningMadolWindow(SCEDULE_BLOCK, REDACT_SCEDULE_BTN, SCEDULE_MODAL_WINDOW);
    }

    public void isContactsModalWindowDisplayed() {
        checkingOpeningMadolWindow(CONTACTS_BLOCK, REDACT_CONTACT_BTN, CONTACTS_MODAL_WINDOW);
    }

    public void isDeviceModalWindowDisplayed() {
        checkingOpeningMadolWindow(DEVICE_BLOCK, ADD_DEVICE_BTN, ADD_DEVICE_MODAL_WINDOW);
    }

    public void checkingOpeningMadolWindow(By bllock, By bnt, By modalWindow){
        scrollIntoView(getElement(bllock));
        click(bnt);
        shortWait.until(ExpectedConditions.visibilityOfElementLocated(modalWindow));
        isElementPresent(modalWindow);
        new Actions(driver).sendKeys(Keys.ESCAPE).perform();
    }

    public String getPosition() {
        return getTextByLocator(RESUME_POSITION_TEXT);
    }


    public String getShortResume() {
        return getTextByLocator(SHORT_RESUME_TEXT);
    }

    public void openResumeModalWindow() {
        openModalWindowByLocator(REDACT_RESUME_BTN);
        shortWait.until(ExpectedConditions.visibilityOfElementLocated(RESUME_MODAL_WINDOW));
    }

    private void openModalWindowByLocator(By redactResumeBtn) {
        scrollIntoView(getElement(redactResumeBtn));
        click(redactResumeBtn);
    }

    public void setNewInformationInResumeModalWindow(String position, String shortResume) {
        click(RESUME_POSITION_SELECT);
        click(By.xpath("//select[@id='post-id']//option[contains(text(), '" + position + "')]"));
        fillField(SHORT_RESUME_TEXT_AREA, shortResume);
    }

    public void saveInformationIntoModalWindow(){
        click(SAVE_RESUME_CHANGES);
        try{
            shortWait.until(ExpectedConditions.visibilityOfElementLocated(SUCCESS_MESSAGE));
        } catch (Exception e){
            click(SAVE_RESUME_CHANGES);  // по какой-то странной причине, когда бежит тест один клик, только убирает фокус из textarea
        }
        shortWait.until(ExpectedConditions.visibilityOfElementLocated(SUCCESS_MESSAGE));
    }

    public void closeModalWindowByXBtn(){
        click(RESUME_MODAL_CLOSE_BY_X_BTN);
    }

    public void closeModalWindowByCloseWithoutSavingBtn(){
        click(RESUME_MODAL_CLOSE_BY_CLOSE_BTN);
    }

    public String getPositionFromModalWindow() {
        return getTextByLocator(RESUME_POSITION_SELECT).trim();
    }

    public String getShortResumeFromModalWindow() {
        return getTextByLocator(SHORT_RESUME_TEXT_AREA);
    }

    public void openPreviewResume() {
        String mainHandler = BaseClass.getMainWindowHandler();

        waitAndSwitchToNewWindow(getElement(GET_RESUME_PREVIEW_BTN));
        String CurrentPageUrl = getCurrentUrl();
        assertTrue("Wrong page address", CurrentPageUrl.contains("/preview"));
        closeNewBrowserTab(mainHandler);

    }

    private void closeNewBrowserTab(String handler) {
        Set<String> windowHandles = driver.getWindowHandles();
        windowHandles.remove(handler);
        for (String handle : windowHandles) {
            driver.switchTo().window(handle);
            driver.close();
        }
        try {
            driver.switchTo().window(handler);
        } catch (Exception e) {
            //
        }
    }

    private void waitAndSwitchToNewWindow(WebElement clickTarget) {
        final Set<String> oldWindowSet = driver.getWindowHandles();
        sleep(500);
        clickTarget.click();
        waitForWindow(oldWindowSet);
    }

    private String waitForWindow(final Set<String> oldWindowSet) {
        String windowHandler = null;
        try {
            windowHandler = (new WebDriverWait(driver, Timeout.defaultTimeout)).until(new ExpectedCondition<String>() {
                public String apply(WebDriver driver){
                    Set<String> newWindowsSet = driver.getWindowHandles();
                    String handler = "";
                    newWindowsSet.removeAll(oldWindowSet);
                    if (newWindowsSet.size() == 1) {
                        handler = newWindowsSet.iterator().next();
                    }
                    return handler.equals("") ? null : handler;
                }
            });
        } catch (TimeoutException te){
            fail("New window hasn't opened");
        }
        driver.switchTo().window(windowHandler);
        sleep(4500); //for content loading and redirecting
        return windowHandler;
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }
}
