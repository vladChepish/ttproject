package lib.BaseClass;

import io.github.bonigarcia.wdm.WebDriverManager;
import lib.obj.User;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.BasePage;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {

    public static WebDriver webDriver;
    public static String devUrl;
    protected static Properties properties;
    public static User user1;
    protected static final ThreadLocal<String> mainWindowHandler = new InheritableThreadLocal<>();

    @BeforeClass
    public static void setUp(){
        setProperties();
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.get(devUrl);
        BasePage page = new BasePage(webDriver);
    }

    private static synchronized void setProperties() {
        properties = new Properties();
        try{
            properties.load(new InputStreamReader(BaseClass.class.getClassLoader().getResourceAsStream("config.properties"), "windows-1251"));
        } catch (IOException e){
            System.out.println("Can't load 'config.properties'");
        }

        devUrl = properties.getProperty("devUrl");

        /** Initialize user1 */
        user1 = new User(properties.getProperty("userName1"),
                properties.getProperty("password1"));
    }

    public static String getMainWindowHandler() {
        return mainWindowHandler.get();
    }

    @AfterClass
    public static void tearDown(){
        webDriver.quit();
    }

}
