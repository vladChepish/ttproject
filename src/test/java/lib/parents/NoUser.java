package lib.parents;

import lib.BaseClass.BaseClass;
import org.junit.BeforeClass;
import pages.MainPage;

public class NoUser extends BaseClass{

    protected static MainPage mainPage;

    @BeforeClass
    public static void beforeClassMethod(){
        mainPage = new MainPage(webDriver);
    }

}
