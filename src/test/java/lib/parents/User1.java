package lib.parents;

import lib.BaseClass.BaseClass;
import org.junit.BeforeClass;
import pages.MainPage;
import pages.MyProfilePage;

public class User1 extends BaseClass {

    protected static MainPage mainPage;
    protected static MyProfilePage profilePage;

    @BeforeClass
    public static void beforeClassMethod(){
        mainPage = new MainPage(webDriver);
        mainPage.login(user1.getUserName(), user1.getPassword());
    }
}
